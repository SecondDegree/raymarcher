﻿using Unity.Mathematics;
using UnityEngine;

public static class ExtensionMethods
{
    public static float Max(this Vector3 vec) => Mathf.Max(vec.x, vec.y, vec.z);

    public static Vector3 ToVector3(this float f) => new Vector3(f, f, f);

    public static Vector3 Abs(this Vector3 vec) => new Vector3(Mathf.Abs(vec.x), Mathf.Abs(vec.y), Mathf.Abs(vec.z));

    public static Color ToColor(this float3 vec) => new Color(vec.x, vec.y, vec.z);
}
