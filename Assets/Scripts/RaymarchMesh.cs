﻿using Unity.Mathematics;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class RaymarchMesh : MonoBehaviour
{    
    public struct RaymarchObjectData
    {
        public float3 Position;
        public quaternion Rotation;
        public float3 Size;
        public MeshShape Type;
        public InteractionMode Mode;
        public float3 Color;
        public float blendDist;

        public float Radius => Size.x;
    }

    public enum MeshShape
    {
        Sphere,
        Cube,
        Torus,
        TriPrism,
    }

    /// <summary>
    /// Describes how this mesh will interact with other meshes
    /// </summary>
    public enum InteractionMode
    {
        // Render the closest SDF.
        Union,
        // Render the farthest SDF.
        Subtract,
        // Render the farthest SDF that is within this SDF.
        Intersect,
    }

    public MeshShape Type;
    public InteractionMode Mode;
    public Color Color = Color.white;
    [Range(.0001f, 1f)]
    public float BlendDistance;

    public Vector3 Position => transform.position;
    public Quaternion Rotation => transform.rotation;
    public Vector3 Scale => transform.localScale;

    public RaymarchObjectData ToData() => new RaymarchObjectData { Position = Position, Rotation = Rotation, Size = Scale,
                                                                   Type = Type, Mode = Mode,
                                                                   Color = new float3(Color.r, Color.g, Color.b),
                                                                   blendDist = BlendDistance };

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.matrix = transform.localToWorldMatrix;
        switch (Type)
        {
            case MeshShape.Sphere:
            case MeshShape.Torus:
                Gizmos.DrawWireSphere(Vector3.zero, 0.5f);
                break;
            case MeshShape.Cube:
            case MeshShape.TriPrism:
                Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
                break;
        }
    }
}
