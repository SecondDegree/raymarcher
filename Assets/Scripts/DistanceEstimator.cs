﻿using Unity.Mathematics;

// Distance Estimated Fractals: http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
// Distance estimator equations: https://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm

public static class Estimators
{
    // Operations
    public static float3 OpTranslateRotate(float3 p, float3 position, quaternion rotation)
    {
        return math.mul(math.inverse(rotation), p - position);
    }

    public static float3 OpRepeat(float3 p, float3 repetition)
    {
        return math.fmod(p + 0.5f * repetition, repetition) - 0.5f * repetition;
    }

    // Shapes
    public static float Sphere(float3 p, float radius)
    {
        return math.length(p) - radius;
    }

    public static float Cube(float3 p, float3 size)
    {
        float3 q = math.abs(p) - size;
        return math.length(math.max(q, 0)) + math.min(math.max(q.x, math.max(q.y, q.z)), 0);
    }

    public static float Torus(float3 p, float2 t)
    {
        var q = new float2(math.length(p.xz) - t.x, p.y);
        return math.length(q) - t.y;
    }

    public static float TriangularPrism(float3 p, float2 h)
    {
        var q = math.abs(p);
        return math.max(q.z - h.y, math.max(q.x * 0.866025f + p.y * 0.5f, -p.y) - h.x * 0.5f);
    }

    public static float Mandelbulb(float3 p, int iterations = 15, float pow = 1.5f)
    {
        // http://blog.hvidtfeldts.net/index.php/2011/09/distance-estimated-3d-fractals-v-the-mandelbulb-different-de-approximations/
        var z = p;
        float dr = 1;
        float r = 0;
        for(int i = 0; i < iterations; ++i)
        {
            r = math.length(z);
            if (r > 2) break;

            // Convert to polar coordinates
            var theta = math.acos(z.z / r);
            var phi = math.atan2(z.y, z.x);
            dr = math.pow(r, pow - 1) * pow * dr + 1;

            // Scale and rotate the point
            var zr = math.pow(r, pow);
            theta *= pow;
            phi *= pow;

            // Convert back to cartesian coordinates
            z = zr * new float3(math.sin(theta) * math.cos(phi), math.sin(phi) * math.sin(theta), math.cos(theta));
            z += p;
        }
        return 0.5f * math.log(r) * r / dr;
    }

    public static float SierpinskiTriangle(float3 point, int iterations, float size)
    {
        var a1 = new float3(1, 1, 1);
        var a2 = new float3(-1, -1, 1);
        var a3 = new float3(1, -1, -1);
        var a4 = new float3(-1, 1, -1);
        float3 c;
        int n = 0;
        float dist, d;
        while (n < iterations)
        {
            c = a1;
            dist = math.distance(point, a1);

            d = math.distance(point, a2);
            if (d < dist)
            {
                c = a2;
                dist = d;
            }

            d = math.distance(point, a3);
            if (d < dist)
            {
                c = a3;
                dist = d;
            }

            d = math.distance(point, a4);
            if (d < dist)
            {
                c = a4;
                dist = d;
            }
            point = size * point - c * (size - 1f);

            n++;
        }

        return math.length(point) * math.pow(size, -n);
    }
}
