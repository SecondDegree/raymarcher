﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Profiling;
using UnityEngine.UI;

// Following this tutorial: https://adrianb.io/2016/10/01/raymarching.html#implementing-a-basic-raymarcher

[ExecuteInEditMode]
[ImageEffectAllowedInSceneView]
[RequireComponent(typeof(Camera))]
public class RaymarchCamera : SceneViewFilter
{
    [SerializeField, ReadOnlyInspector]
    private Shader _effectShader;
    [SerializeField, ReadOnlyInspector]
    private Material _effectMaterial;
    [SerializeField, ReadOnlyInspector]
    private Camera _currentCamera;

    [SerializeField, Range(2, 100)]
    private int MaximumRaySteps = 20;
    [SerializeField, Range(0.00001f, 0.1f)]
    private float MinimumDistance = 0.05f;

    [SerializeField, Range(20, 512)]
    private int _width = 50;
    [SerializeField, Range(20, 512)]
    private int _height = 50;

    [SerializeField, Range(1.01f, 5f)]
    private float pow = 1.5f;
    [SerializeField, Range(0.01f, 5f)]
    private float powRate = 0.1f;

    [SerializeField, Range(1, 30)]
    private int iterations = 15;

    private RenderTexture _renderTexture;
    private Texture2D _tex;

    public bool HasShader => _effectShader != null;
    public Material EffectMaterial => _effectMaterial;

    private RenderTexture RT
    {
        get
        {
            if(_renderTexture == null || _renderTexture.width != _width || _renderTexture.height != _height)
            {
                if (_renderTexture != null) _renderTexture.Release();

                _renderTexture = new RenderTexture(_width, _height, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
                _renderTexture.enableRandomWrite = true;
                _renderTexture.hideFlags = HideFlags.HideAndDontSave;
                _renderTexture.Create();
            }

            return _renderTexture;
        }
    }

    private void Update()
    {
        if(Application.isPlaying)
        {
            pow += powRate * Time.deltaTime;
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Profiler.BeginSample("RayMarch Camera - OnRenderImage");
        _currentCamera = Camera.current;

        // Collect Scene
        Profiler.BeginSample("Collect Scene");
        NativeArray<RaymarchMesh.RaymarchObjectData> scene;
        {
            var meshes = FindObjectsOfType<RaymarchMesh>();
            var sceneObjects = new RaymarchMesh.RaymarchObjectData[meshes.Length];
            for (int i = 0; i < meshes.Length; ++i)
            {
                sceneObjects[i] = meshes[i].ToData();
            }
            Array.Sort(sceneObjects, (a, b) => a.Mode.CompareTo(b.Mode) );

            scene = new NativeArray<RaymarchMesh.RaymarchObjectData>(sceneObjects, Allocator.TempJob);
        }
        Profiler.EndSample();

        if (_tex == null || _tex.width != RT.width || _tex.height != RT.height)
        {
            Profiler.BeginSample("Re-create texture");
            _tex = new Texture2D(RT.width, RT.height) { hideFlags = HideFlags.HideAndDontSave };
            Profiler.EndSample();
        }

        var viewPos = _currentCamera.transform.position;
        // Inversed view projection matrix
        float4x4 invViewProjMatrix = math.inverse(_currentCamera.projectionMatrix * _currentCamera.worldToCameraMatrix);

        int2 renderSize = new int2(RT.width, RT.height);
        int totalPixels = renderSize.x * renderSize.y;
        var jobResults = new NativeArray<float4>(totalPixels, Allocator.TempJob);

        Profiler.BeginSample("Start jobs");
        TraceJob traceJob = new TraceJob { ViewPosition = viewPos, MaximumRaySteps = MaximumRaySteps, MinimumDistance = MinimumDistance,
                                           RenderSize = renderSize, InvViewProjMatrix = invViewProjMatrix , Scene = scene, Results = jobResults,
                                            Iterations = iterations, Power = pow };
        var jobHandle = traceJob.Schedule(totalPixels, totalPixels / math.max(renderSize.x, renderSize.y));
        Profiler.EndSample();

        // Extract job results into pixels
        Profiler.BeginSample("Complete job");
        jobHandle.Complete();
        for (var i = 0; i < totalPixels; ++i)
        {
            var y = i / renderSize.x;
            var x = i % renderSize.x;
            _tex.SetPixel(x, y, jobResults[i].xyz.ToColor() * jobResults[i].w);
        }
        scene.Dispose();
        jobResults.Dispose();
        Profiler.EndSample();

        _tex.Apply();

        Graphics.Blit(_tex, RT);

        Graphics.Blit(RT, destination);
        Profiler.EndSample();
    }

#if UNITY_EDITOR
    public void UpdateInternalVariables()
    {
        if(_effectShader != null && _effectMaterial == null)
        {
            _effectMaterial = new Material(_effectShader);
            _effectMaterial.hideFlags = HideFlags.HideAndDontSave;
        }

        _currentCamera = Camera.current;
    }
#endif
}


[BurstCompile]
public struct TraceJob : IJobParallelFor
{
    public float3 ViewPosition;
    public int MaximumRaySteps;
    public float MinimumDistance;
    public int2 RenderSize;
    public float4x4 InvViewProjMatrix;
    [ReadOnly]
    public NativeArray<RaymarchMesh.RaymarchObjectData> Scene;
    [WriteOnly]
    public NativeArray<float4> Results;
    public int Iterations;
    public float Power;

    public void Execute(int index)
    {
        // Calculate x and y from index
        var y = index / RenderSize.x;
        var x = index % RenderSize.x;

        var direction = PixelToWorldDir(x, y);

        Results[index] = Trace(direction);
    }

    private float3 PixelToWorldDir(float x, float y)
    {
        // I used this https://stackoverflow.com/questions/7692988/opengl-math-projecting-screen-space-to-world-space-coords

        // Remap from pixel space to heterogeneous (-1 to 1) space
        var xp = x / RenderSize.x * 2f - 1f;
        var yp = y / RenderSize.y * 2f - 1f;

        var projected = math.mul(InvViewProjMatrix, new float4(xp, yp, 1, 1));

        // This should now be a world space position on the far plane.
        projected.xyz /= projected.w;

        return math.normalize(projected.xyz - ViewPosition);
    }

    public float4 Trace(float3 direction)
    {
        float currentDistance = 0f;
        float3 currentColor = 1;

        int steps;
        for (steps = 0; steps < MaximumRaySteps; steps++)
        {
            Vector3 p = ViewPosition + direction * currentDistance;
            var estimate = EstimateScene(p);
            if (estimate.w < MinimumDistance) break;
            currentDistance += estimate.w;
            currentColor = estimate.xyz;
        }

        return new float4(currentColor, 1.0f - (float)steps / MaximumRaySteps);
    }

    private float4 EstimateScene(float3 p)
    {
        // Estimate is packed data: xyz = color, w = distance
        float4 estimate = new float4(p, 1);

        for (int i = 0; i < Scene.Length; ++i)
        {
            var obj = Scene[i];
            var pt = Estimators.OpTranslateRotate(p, obj.Position, obj.Rotation);
            var dist = ShapeDist(pt, in obj);

            estimate = Combine(estimate.w, dist, estimate.xyz, obj.Color, obj.blendDist, obj.Mode);
        }

        return estimate;
    }

    private static float ShapeDist(float3 p, in RaymarchMesh.RaymarchObjectData shape)
    {
        switch (shape.Type)
        {
            case RaymarchMesh.MeshShape.Sphere:
                return Estimators.Sphere(p, shape.Radius / 2);
                //return Estimators.Mandelbulb(p, Iterations, Power);
            case RaymarchMesh.MeshShape.Cube:
                return Estimators.Cube(p, shape.Size / 2);
            case RaymarchMesh.MeshShape.Torus:
                return Estimators.Torus(p, shape.Size.xy / 2);
            case RaymarchMesh.MeshShape.TriPrism:
                return Estimators.TriangularPrism(p, shape.Size.yz / 2);
        }

        return float.PositiveInfinity;
    }

    // TODO: Take in a blend distance for each object. Right now only the last processed blend distance matters. Probably just use whichever is biggest.
    private static float4 Combine(float distA, float distB,
                                  float3 colorA, float3 colorB,
                                  float blendDist, RaymarchMesh.InteractionMode mode)
    {
        // https://www.iquilezles.org/www/articles/smin/smin.htm
        // Smooth minumim & blend factor. Returns smooth-minimum in x and blend factor in y.
        static float2 smin(float a, float b, float k)
        {
            // Quadratic smooth min
            float h = math.max(k - math.abs(a - b), 0f) / k;
            float m = h * h * 1f / 2f;
            float s = m * k * 1f / 2f;
            return a < b ? new float2(a - s, m) : new float2(b - s, 1f - m);
        }

        switch (mode)
        {
            case RaymarchMesh.InteractionMode.Subtract:
                // TODO: Blending
                if (-distB > distA)
                    return new float4(colorB, -distB);
                break;
            case RaymarchMesh.InteractionMode.Intersect:
                // TODO: Blending?
                if (distB > distA)
                    return new float4(colorB, distB);
                break;
            default: // Default is Union
                // TODO: Skip if blendDist is == 0 to avoid divide by zero! Fall back to basic min check.
                var blendInfo = smin(distB, distA, blendDist);
                var blendedColor = math.lerp(colorB, colorA, blendInfo.y);

                return new float4(blendedColor, blendInfo.x);
        }

        return new float4(colorA, distA);
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(RaymarchCamera))]
public class RaymarchCameraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var filter = (RaymarchCamera)target;

        if(GUILayout.Button("Update Internal Variables"))
        {
            filter.UpdateInternalVariables();
        }
    }
}
#endif